// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define(['jquery', 'core_message/message_drawer_helper'], function($, MessageDrawer) {
    return {
        init: function(userid, contact) {
            $('.block.block_sharing_cart .content').append(
                    $('<button>').attr('type', 'button')
                    .addClass('btn btn-secondary d-block mx-auto mt-1')
                    .text(M.util.get_string('sendamessage', 'block_sharing_cart'))
                    .prepend($('<i class="icon fa fa-comment"></i>'))
                    .click(function() {
                        if (typeof MessageDrawer.hide !== 'undefined') {
                            // In Moodle 3.8+, message API is different.
                            MessageDrawer.createConversationWithUser({userid: contact});
                        } else {
                            MessageDrawer.createConversationWithUser(contact);
                        }
                        var $messagearea = $('textarea[data-region="send-message-txt"]');
                        if ($messagearea.val() == '') {
                            var template = M.util.get_string('messagetemplate', 'block_sharing_cart');
                            $('textarea[data-region="send-message-txt"]').val(template).text(template);
                        }
                    })
            );
        }
    };
});
